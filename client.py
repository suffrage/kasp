# -*- coding: utf-8 -*-

import argparse
import requests
from common.validator import Validator
from common.parser import Parser
from common.serializator import Serializator

from pprint import pprint

"""
Клиент
Приложение читает из файла все строки содержащие <md5>&&&<строка-имя файла>, упаковывает эти данные в структуру xml или json и отправляет все данные из файла одним http post-запросом на сервер.
После получения ответа от сервера Приложение выводит его на экран.

Должен запускаться из командной строки
Должен иметь следующие параметры:
                - ip адрес сервера
                - порт сервера
                - формат: xml|json
                - путь к файлу с данными
"""

class Client(object):

    def __init__(self):

        parser = argparse.ArgumentParser(description='Client read file with <md5>&&&<строка-имя файла> strings \n'
                                                     'pack it to xml or json and send to server')
        parser.add_argument('--serialize_type',
                            dest='serialize_type',
                            choices=['xml', 'json'],
                            required=True,
                            help="serialize type"
                            )
        parser.add_argument('--server',
                            dest='server_ip',
                            type=Validator.ip,
                            required=True,
                            help="server ipv4 or ipv6 address"
                            )
        parser.add_argument('--port',
                            dest='server_port',
                            type=Validator.port,
                            required=True,
                            help="server port 1025-65535"
                            )
        parser.add_argument('--file',
                            dest='file_path',
                            required=True,
                            type=Validator.file_exists,
                            help="path to file"
                            )

        args = parser.parse_args()

        self.serialize_type = args.serialize_type
        self.server_ip = args.server_ip
        self.server_port = args.server_port
        self.file_path = args.file_path

    def send_request(self, data):
        if self.serialize_type == "xml":
            headers = {'Content-Type': 'application/xml'}
        if self.serialize_type == "json":
            headers = {'Content-Type': 'application/json'}
        try:
            r = requests.post('http://{}:{}'.format(self.server_ip, self.server_port), data=data, headers=headers)
        except requests.exceptions.ConnectionError as e:
            print "Error: {}".format(e)
            return None, None

        return r.status_code, r.text

    def pack_data(self):
        return Serializator.serialize(Parser.parse_file(self.file_path), self.serialize_type)

    def unpack_data(self, data):
        return Serializator.deserialize(data, self.serialize_type)


if __name__ == "__main__":
    client = Client()
    data = client.pack_data()
    status_code, answer = client.send_request(data)

    if status_code:
        if status_code == 200:
            pprint(client.unpack_data(answer))
        elif status_code == 415:
            print "Error. Status code: 415. Unsupported media Type"
        else:
            print "Error. Status code: {}. {}".format(status_code, answer)