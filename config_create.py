import ConfigParser

config = ConfigParser.RawConfigParser()

config.add_section('ServerConfig')
config.set('ServerConfig', 'host_name', '127.0.0.1')
config.set('ServerConfig', 'port', '9000')
config.set('ServerConfig', 'md5_file', 'test_files/md5_file.txt')
config.set('ServerConfig', 'string_file', 'test_files/string_file.txt')

with open('server.cfg', 'wb') as configfile:
    config.write(configfile)