# -*- coding: utf-8 -*-

import time
import BaseHTTPServer
import socket
import ConfigParser
from common.serializator import Serializator
from common.server import Server

class ServerHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        html = """
            <html>
                <head>
                    <title>Example server</title>
                </head>
                <body>
                    Друзья все валят за границу, <br />
                    А я, наверное, остаюсь. <br />
                    Смеюсь, напьюсь, если фигова, <br />
                    Жрать будет нечего - женюсь. <br />
                    Друзья мне пишут, как ты тут. <br />
                    А я нормально - супер гуд! <br />
                </body>
            </html>
            """
        s.wfile.write(html)
    def do_POST(s):

        content_type =Server.get_content_type(s.headers.getheader('Content-Type'))
        if content_type:

            content_len = int(s.headers.getheader('content-length', 0))
            post_body = s.rfile.read(content_len)
            data = Serializator.deserialize(post_body, content_type)
            answer_data = Server.search(data, MD5_FILE, STRING_FILE)
            answer = Serializator.serialize(answer_data, content_type)
            print answer

            s.send_response(200)
            s.send_header("Content-type", content_type)
            s.end_headers()
            s.wfile.write(answer)
        else:
            s.send_response(415)


if __name__ == '__main__':
    try:

        server_config_file = "server.cfg"
        Config = ConfigParser.ConfigParser()
        Config.read(server_config_file)

        section = "ServerConfig"
        HOST_NAME = Config.get(section, "host_name")
        PORT_NUMBER = int(Config.get(section, "port"))
        MD5_FILE = Config.get(section, "md5_file")
        STRING_FILE = Config.get(section, "string_file")

        server_class = BaseHTTPServer.HTTPServer
        httpd = server_class((HOST_NAME, PORT_NUMBER), ServerHandler)
        print time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER)

        httpd.serve_forever()
    except socket.error as e:
        print e
    except ConfigParser.NoSectionError as e:
        print "Configuration error. Please check configuration file {}".format(server_config_file)
    except KeyboardInterrupt:
        httpd.server_close()
        print time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER)
    except Exception as e:
        print "Error: {}".format(e)