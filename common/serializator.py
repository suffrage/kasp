# -*- coding: utf-8 -*-

import json
import yaml
from dict2xml import dict2xml
import xmltodict
from common.exceptions import SerializeTypeError


class Serializator():

	def __init__(self):
		pass

	@classmethod
	def serialize(cls, data, type):
		if not isinstance(data, dict):
			raise TypeError("Must be dictionary not {}".format(type(data)))
		if type not in ['json', 'xml']:
			raise SerializeTypeError("Type must be json or xml")
		if type == 'json':
			return json.dumps(data)
		elif type == 'xml':
			return dict2xml(data, 'items')

	# c yaml здесь костыль, для того чтобы избавиться от unicode в строке
	@classmethod
	def deserialize(cls, data, type):
		if type not in ['json', 'xml']:
			raise SerializeTypeError("Type must be json or xml")
		if type == 'json':
			return yaml.safe_load(data)
			# return json.loads(data)
		elif type == 'xml':
			# заменяем "'None'", "None" чтобы работало одинаково
			xml_json = json.dumps(xmltodict.parse(data)).replace("\"None\"", "null")
			return yaml.safe_load(xml_json)["items"]
			# return json.loads(json.dumps(xmltodict.parse(data)))["items"]