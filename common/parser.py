# -*- coding: utf-8 -*-

import re

class Parser():

    def __init__(self):
        pass

    @classmethod
    def parse_string(cls, string):

        pattern =  '^([a-f0-9]{32})\&\&\&([\w]*\.[\w]{0,3})$';

        pattern = re.compile(pattern, re.IGNORECASE)
        if pattern.match(string):
            data = re.findall(pattern, string)
            return data[0][0], data[0][1]
        else:
            return None

    @classmethod
    def parse_file(cls, file_path):
        with open(file_path) as f:
            lines = f.readlines()
        result = {}
        for line in lines:
            parsed_line = cls.parse_string(line)
            if parsed_line:
                result[parsed_line[0]] = { "file": parsed_line[1] }
        return result

