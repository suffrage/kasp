# -*- coding: utf-8 -*-

import unittest
import json
from dict2xml import dict2xml
from common.serializator import Serializator
from common.exceptions import *


class SerializatorTestCase(unittest.TestCase):
	def setUp(self):

		self.right_data = {
			'a123456789abcdef0123456789abcdef': {'file': 'notepad.exe'},
			'a000456789abcdef0123456789abcdef': {'file': 'winword.exe'}
		}

		self.serialize_types = ['xml', 'json']

	def test_CheckData(self):

		# right_data = '{ "0123456789abcdef0123456789abcdef": {"file": "notepad.exe"}, ' \
		#              '"0000456789abcdef0123456789abcdef": {"file": "winword.exe"}}{"0123456789abcdef0123456789abcdef": {"file_value": null, "hash_value": null, "file": "notepad.exe"}, ' \
		#              '"0000456789abcdef0123456789abcdef": {"file_value": null, "hash_value": "odinodin", "file": "winword.exe"}}')
		# Serializator.deserialize()

		pass

	def test_SerializeDeserialize(self):

		for serialize_type in self.serialize_types:
			serialized_data = Serializator.serialize(self.right_data, serialize_type)
			deserialized_data = Serializator.deserialize(serialized_data, serialize_type)
			self.assertEqual(self.right_data, deserialized_data)

	def test_CheckWrongSerializeType(self):

		wrong_types = ["jsonp", {}, ["aa", "aa"], "xm"]

		for wrong_type in wrong_types:
			with self.assertRaises(SerializeTypeError):
				Serializator.serialize(self.right_data, wrong_type)

	def test_CheckRightSerializeType(self):

		for right_type in self.serialize_types:
			Serializator.serialize(self.right_data, right_type)

	def test_CheckSerialize(self):

		self.assertEqual(Serializator.serialize(self.right_data, 'json'), json.dumps(self.right_data))
		self.assertEqual(Serializator.serialize(self.right_data, 'xml'), dict2xml(self.right_data, 'items'))

	def test_CheckDataType(self):

		wrong_data = ['example', '{}', []]

		Serializator.serialize(self.right_data, 'json')
		for data in wrong_data:
			with self.assertRaises(TypeError):
				Serializator.serialize(data, 'json')


if __name__ == '__main__':
	unittest.main()
