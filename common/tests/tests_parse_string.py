# -*- coding: utf-8 -*-

import unittest

from common.parser import Parser


class ParseStringTestCase(unittest.TestCase):

	def test_CheckRightStringCheck(self):

		test_string = Parser.parse_string("0123456789abcdef0123456789abcdef&&&notepad.exe")
		parse_string = ('0123456789abcdef0123456789abcdef', 'notepad.exe')

		self.assertEqual(test_string, parse_string)


	def test_CheckWrongStringCheck(self):

		test_string = Parser.parse_string("adasdhgadasjhg&&&winword.exe&&&notepad.exe")

		self.assertIsNone(test_string)



if __name__ == '__main__':
	unittest.main()