# -*- coding: utf-8 -*-

class Server():
	def __init__(self):
		pass

	@classmethod
	def get_content_type(cls, request_content_type):

		supported_content_types = ["application/json", "application/xml"]
		if request_content_type in supported_content_types:
			return request_content_type.split("/")[1]
		else:
			return None

	@classmethod
	def search(cls, data, hash_file, string_file):

		data = cls.remap(data)

		print "open {}".format(hash_file)

		hash_items = data.keys()

		with open(hash_file, 'r') as f:
			for line in f:
				print line
				for hash_item in hash_items:
					if hash_item in line:
						data[hash_item]["hash_value"] = line.split(":")[1].rstrip()
						hash_items.remove(hash_item)
				if not hash_items:
					break

		hash_items = data.keys()

		print "open {}".format(string_file)
		with open(string_file, 'r') as f:
			for line in f:
				print line
				for hash_item in hash_items:
					if data[hash_item]["file"] in line:
						data[hash_item]["string_value"] = line.split(":")[1].rstrip()
						hash_items.remove(hash_item)
				if not hash_items:
					break

		return data



	@classmethod
	def remap(cls, data):
		for key in data.keys():
			data[key]["hash_value"] = None
			data[key]["file_value"] = None
		return data
