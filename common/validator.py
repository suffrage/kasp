# -*- coding: utf-8 -*-

import ipaddress
import os.path
from argparse import ArgumentTypeError


class Validator:

    @classmethod
    def port(cls, string):
        if not string.isdigit():
            raise ArgumentTypeError("Port must be integer")
        else:
            port = int(string)
            if not 1024 < port <= 65535:
                raise ArgumentTypeError("Please use port from 1025-65535 range")
            else:
                return port

    @classmethod
    def ip(cls, string):
        try:
            ipaddress.ip_address(unicode(string))
            return string
        except ValueError as e:
            raise ArgumentTypeError(e)
        except:
            raise ArgumentTypeError("Error in server_ip validate")

    @classmethod
    def file_exists(cls, string):
        if os.path.isfile(string):
            return string
        else:
            raise ArgumentTypeError("File not exists")