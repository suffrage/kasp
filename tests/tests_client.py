# -*- coding: utf-8 -*-

import unittest
import mock
from client import Client
from common.serializator import Serializator
from pprint import pprint

class Resp(object):
    def __init__(self, status_code, text):
        self.text = text
        self.status_code = status_code

class FakeClient(Client):

            def __init__(self):
                self.serialize_type = "json"
                self.server_ip = "127.0.0.1"
                self.server_port = 9000
                self.file_path = "test_files/test_file.txt"

class ClientTestCase(unittest.TestCase):

    def setUp(self):
        self.right_data = {
            'a123456789abcdef0123456789abcdef': {'file': 'notepad.exe'},
            'a000456789abcdef0123456789abcdef': {'file': 'winword.exe'}
        }

        self.xml = """
                    <items>
                        <a000456789abcdef0123456789abcdef>
                            <file>winword.exe</file>
                        </a000456789abcdef0123456789abcdef>

                        <a123456789abcdef0123456789abcdef>
                            <file>notepad.exe</file>
                        </a123456789abcdef0123456789abcdef>
                    </items>
                    """

        self.right_answer = {
            "a000456789abcdef0123456789abcdef": {'file': 'winword.exe',
                                               'file_value': None,
                                               'hash_value': 'odinodin',
                                               'string_value': 'dvadva'},
            "a123456789abcdef0123456789abcdef": {'file': 'notepad.exe',
                                               'file_value': None,
                                               'hash_value': None}
        }

        self.right_answer_json = """
            {
                "a123456789abcdef0123456789abcdef": {"file_value": null, "hash_value": null, "file": "notepad.exe"},
                "a000456789abcdef0123456789abcdef": {"file_value": null, "hash_value": "odinodin", "string_value": "dvadva", "file": "winword.exe"}
            }
        """

        self.right_answer_xml = """
            <items>
              <a000456789abcdef0123456789abcdef>
                <file>winword.exe</file>
                <file_value>None</file_value>
                <hash_value>odinodin</hash_value>
                <string_value>dvadva
                </string_value>
              </a000456789abcdef0123456789abcdef>
              <a123456789abcdef0123456789abcdef>
                <file>notepad.exe</file>
                <file_value>None</file_value>
                <hash_value>None</hash_value>
              </a123456789abcdef0123456789abcdef>
            </items>
        """

        self.serialize_types = ['xml', 'json']



    @mock.patch('requests.post')
    def test_SendRequest(self, mock_responce):

        for serialize_type in self.serialize_types:
            responce = Resp(200, self.right_answer)
            mock_responce.return_value = responce

            client = FakeClient()
            client.serialize_type = serialize_type
            # data = client.pack_data(self.right_data)
            data = Serializator.serialize(self.right_data, serialize_type)
            status_code, answer = client.send_request(data)
            self.assertEqual(answer, self.right_answer)

    def test_unpackData(self):
        client = FakeClient()
        self.maxDiff = None

        client.serialize_type = "xml"
        self.assertEqual(client.unpack_data(self.right_answer_xml), self.right_answer)

        client.serialize_type = "json"
        self.assertEqual(client.unpack_data(self.right_answer_json), self.right_answer)

